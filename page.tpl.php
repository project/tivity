<?php
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language ?>" lang="<?php print $language->language ?>" dir="<?php print $language->dir ?>">
  <head>
    <title><?php print $head_title ?></title>
    <?php print $head ?>
    <?php print $styles ?>
    <?php print $scripts ?>
    <!--[if lt IE 7]>
      <?php print phptemplate_get_ie_styles(); ?>
    <![endif]-->
  </head>  
	<body>
		<div id="header">  
				<?php 
	      	if ($logo) {
          	print '<div id="logo"><img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" /></div>';
          }
				?>
			<div id="sitename">
				<?php 
			      print '<h1><a href="'. check_url($front_page) .'" title="'. $site_title .'">' . $site_name . '</a></h1>';
            print '<span class="site_slogan">' . $site_slogan . '</span>';
				?></div>
				<div id="mainmenu">  
				  <?php if (isset($primary_links)) : ?>
	          <?php print theme('links', $primary_links, array('class' => 'links primary-links')) ?>
	        <?php endif; ?>
	        <?php if (isset($secondary_links)) : ?>
	          <?php print theme('links', $secondary_links, array('class' => 'links secondary-links')) ?>
	        <?php endif; ?>
				</div>
		</div>

		<div id="main">
			<div id="left">
	      <?php if ($left): ?>
	        <div class="sidebar">
	          <?php print $left ?>
	        </div>
	      <?php endif; ?>			
			</div>
			<div id="right">
				<div id="inner-right">
				  <?php print $breadcrumb; ?>
          <?php if ($mission): print '<div id="mission">'. $mission .'</div>'; endif; ?>
          <?php if ($tabs): print '<div id="tabs-wrapper" class="clear-block">'; endif; ?>
          <?php if ($title): print '<h2'. ($tabs ? ' class="with-tabs"' : '') .'>'. $title .'</h2>'; endif; ?>
          <?php if ($tabs): print '<ul class="tabs primary">'. $tabs .'</ul></div>'; endif; ?>
          <?php if ($tabs2): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>
          <?php if ($show_messages && $messages): print $messages; endif; ?>
          <?php print $help; ?>
          <div class="clear-block">
            <?php print $content ?>
          </div>
          <?php print $feed_icons ?>
				</div>
			</div>
			<div id="footer">
				<div id="themeby">XHTML/CSS by <a href="http://ivansotof.com" title="IvanSF">IvanSF</a></div>
				<?php print $footer_message . $footer ?>
				<div class="clear"></div>
			</div>
		</div>
	<?php print $closure ?>
	</body>
	
</html>
